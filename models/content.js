const mongoose = require('mongoose');

const HomeContentSchema = new mongoose.Schema({

    banner: [
        {
            product_banner_photo: {
                type: String,
                require: true
            },
            product: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'product',
                required: true
            },
            direction: {
                type: String,
                enum: ["LEFT", "RIGHT"],
                required: true
            },
            order: {
                type: Number,
                min: 1
            }
        }
    ],

    infos =[
        {
            icon: {
                type: String,
                require: true
            },
            text: {
                type: String,
            },
            link: {
                type: String,
            },
            order: {
                type: String,
                min: 1
            }
        }
    ],

    about = {
        photo: {
            type: String,
        },
        title: {
            type: String,
        },
        description: {
            type: String,
        },
        direction: {
            type: String,
            enum: ["LEFT", "RIGHT"],
            required: true
        }
    },
    services: {
        title: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        service: [
            {
                photo: {
                    type: String,
                },
                description: {
                    type: String,
                },
                order: {
                    type: Number,
                    min: 1
                }
            }
        ]

    }
}, { autoCreate: true })

module.exports = mongoose.model('content', HomeContentSchema);
