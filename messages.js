
const MSGS = {
    'CATEGORY404' : 'Caterogia nao encontrada',
    'GENERIC_ERROR': 'Erro!',
    'PRODUCT404' : 'Produto nao encontrado',
    'USER404' : 'Usuario nao encontrado'
}

module.exports = MSGS