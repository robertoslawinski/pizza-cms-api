const express = require('express');
const Category = require('../../models/category');
const { check, validationResult } = require('express-validator');
const router = express.Router();
const MSGS = require('../../messages')

router.get('/:id', [], async (req, res, next) => {
    try {
        const id = req.params.id
        const category = await Category.findOne({ _id: id })
        if (category) {
            res.json(category)
        } else {
            res.status(404).send({ "erro": MSGS.CATEGORY404 })
        }
 
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
})

router.patch('/:id', [], async (req, res, next) => {
    try {
        const category = await Category.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true})
        if (category) {
            res.json(category)
        } else {
            res.status(404).send({ "erro": MSGS.CATEGORY404 })
        }
 
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
})

router.delete('/:id', [], async (req, res, next) => {
    try {
        const id = req.params.id
        const category = await Category.findOneAndDelete({ _id: id })
        if (category) {
            res.json(category)
        } else {
            res.status(404).send({ "erro": MSGS.CATEGORY404 })
        }
 
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
})


router.get('/', async (req, res, next) => {
    try {
        const category = await Category.find({})
        res.json(category)
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "erro": MSGS.GENERIC_ERROR })
    }
})

router.post('/', [
    check('name').not().isEmpty(),
    check('icon').not().isEmpty()],
    async (req, res, next) => {
        try {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            } else {
                let { name, icon } = req.body
                console.log(req.body)
                let category = new Category({ name, icon })
                await category.save()
                if (category.id) {
                    res.json(category);
                }
            }

        } catch (err) {
            console.error(err.message)
            res.status(500).send({ "error": MSGS.GENERIC_ERROR  })
        }
    })

module.exports = router; 
