const express = require('express');
const Product = require('../../models/product');
const { check, validationResult } = require('express-validator');
const router = express.Router();
const MSGS = require('../../messages')
 
router.get('/:id', [], async (req, res, next) => {
    try {
        const id = req.params.id
        const product = await Product.findOne({ _id: id })
        if (product) {
            res.json(product)
        } else {
            res.status(404).send({ "erro": MSGS.PRODUCT404 })
        }
 
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
})

router.patch('/:id', [], async (req, res, next) => {
    try { 
        const product = await Product.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true})
        if (product) {
            res.json(product)
        } else {
            res.status(404).send({ "erro": MSGS.PRODUCT404 })
        }
 
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
})

router.delete('/:id', [], async (req, res, next) => {
    try {
        const id = req.params.id
        const product = await Product.findOneAndDelete({ _id: id })
        if (product) {
            res.json(product)
        } else {
            res.status(404).send({ "erro": MSGS.PRODUCT404 })
        }
 
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
})

router.get('/', async (req, res, next) => {
    try {
        const category = await Product.find({})
        res.json(product)
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "erro": MSGS.GENERIC_ERROR })
    }
})

router.post('/', [
    ], async (req, res, next) => {
        try {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            } else {
                let product = new Product(req.body)
                await product.save()
                if (product.id) {
                    res.json(product);
                }
            }

        } catch (err) {
            console.error(err.message)
            res.status(500).send({ "error": MSGS.GENERIC_ERROR  })
        }
    })

module.exports = router; 
